module Globber (matchGlob) where

import Control.Applicative ((<$>), (<*>))
import Data.List (any, tails)

type GlobPattern = String

type ErrorMessage = String

data GlobToken 
    = Wild
    | WildOne
    | Literal Char
    deriving (Show, Eq)

matchGlob :: GlobPattern -> String -> Bool
matchGlob pattern str = case glob pattern of
    Left _           -> False
    Right globTokens -> match globTokens str

-- | Parse a pattern string into a list of glob tokens.
glob :: String -> Either ErrorMessage [GlobToken]
glob [] = Right []
glob (x:xs) = case x of
    '\\' -> 
        let (eitherToken, rest) = escape xs
        in (:) <$> eitherToken <*> glob rest
    '*'  -> (:) Wild <$> glob xs
    '?'  -> (:) WildOne <$> glob xs
    _    -> (:) (Literal x) <$> glob xs

-- | Parse an escaped character into a globpattern literal.
escape :: String -> (Either ErrorMessage GlobToken, String)
escape [] = (Left "The escape symbol '\' cannot appear at the end of a pattern.", [])
escape (x:xs) = (Right $ Literal x, xs)

match :: [GlobToken] -> String -> Bool
match [] [] = True
match (t:ts) [] = (t == Wild) && (ts == [])
match [] (c:cs) = False
match (t:ts) (c:cs) = case t of
    Wild       -> any (match ts) $ tails (c:cs)
    WildOne    -> match ts cs
    Literal c' -> (c == c') && match ts cs
