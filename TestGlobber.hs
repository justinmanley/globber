module Main (main) where

import Test.Hspec

import Globber

main :: IO ()
main = hspec $ describe "Testing Globber" $ do

    describe "empty pattern" $ do
      it "matches empty string" $
        matchGlob "" "" `shouldBe` True

      it "shouldn't match non-empty string" $
        matchGlob "" "string" `shouldBe` False

    describe "single wildcard" $ do
      it "matches any single character" $ 
        matchGlob "?" "*" `shouldBe` True

      it "matches any single character within a longer string" $
        matchGlob "?s" "xs" `shouldBe` True

    describe "escaping" $ do
      it "matches a literal '?'" $ do
        matchGlob "\\?" "?" `shouldBe` True

      it "matches a literal '*'" $ do
        matchGlob "\\*" "*" `shouldBe` True

      it "only escapes a single character" $ do
        matchGlob "\\*a" "*a" `shouldBe` True

    describe "wildcard" $ do
      it "matches an arbitrary sequence of characters" $ 
        matchGlob "*" "abc?de" `shouldBe` True

      it "matches an arbitrary sequence of characters followed by a literal" $
        matchGlob "*/" "root/" `shouldBe` True

      it "matches an arbitrary sequence of characters in the midst of a set of literals" $
        matchGlob "/*/" "/hi/" `shouldBe` True

      it "matches with more than one '*'" $
        matchGlob "s/*/*/" "s/blah/bleh/" `shouldBe` True

    describe "literal" $ do
      it "matches a sequence of character literals exactly" $
        matchGlob "abcd" "abcd" `shouldBe` True
